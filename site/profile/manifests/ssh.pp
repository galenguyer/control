class profile::ssh {
  user { 'root':
    purge_ssh_keys  => true,
  }

  include ::ssh
}
