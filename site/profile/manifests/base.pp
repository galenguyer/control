class profile::base {
  if ($facts['virtual'] != 'lxc') {
    if ($facts['os']['name'] == 'RedHat' and $facts['os']['release']['major'] >= '8') or
    ($facts['os']['name'] == 'Rocky' and $facts['os']['release']['major'] >= '8') or
    ($facts['os']['name'] == 'Fedora' and $facts['os']['release']['major'] >= '33') or
    ($facts['os']['family'] == 'Debian' and $facts['os']['release']['major'] >= '10') {
      class { 'chrony':
        servers => $ntp_servers,
      }
    } else {
      class { 'ntp':
        servers => $ntp_servers,
      }
    }
  }

  class { 'puppet_agent': }
}
