# Install Puppet on Rocky Linux
```bash
dnf -y install https://yum.puppet.com/puppet-release-el-8.noarch.rpm
dnf update
dnf install puppet-agent
```
