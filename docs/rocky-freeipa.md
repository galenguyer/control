# Install FreeIPA on Rocky Linux
## Server
```bash
dnf module enable idm:DL1
dnf update
dnf module install idm:DL1/server
ipa-server-install [-N|--no-ntp]
```

## Replica
```bash
dnf module enable idm:DL1
dnf update
dnf module install idm:DL1/server
ipa-client-install --domain=antifausa.net --realm=ANTIFAUSA.NET [-N|--no-ntp]
```
Switch to existing master
```bash
ipa hostgroup-add-member ipaservers --hosts ipa02.antifausa.net
```

## Notes?
```
WARNING: The CA service is only installed on one server (ipa01.antifausa.net).
It is strongly recommended to install it on another server.
Run ipa-ca-install(1) on another master to accomplish this.
```
